sudo apt-get install silversearcher-ag vim
git clone https://t2o2@bitbucket.org/t2o2/vim_settings.git ~/vim_settings
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
cd ~/.vim/bundle && git clone https://github.com/rking/ag.vim ag
cp ~/vim_settings/.vimrc ~
